package proydeb;
public class ProyDeb {
    public static void main(String[] args) {
        class CNodo {
	int dato;
	CNodo sig;
        
	public CNodo()	{
            sig = null;
	}
}

class CLista {
    CNodo cabeza;
    public CLista()	{
            cabeza = null;
    }

    public void Insertar(int dat) {
        CNodo NuevoNodo;
        CNodo ant, lue;
        NuevoNodo = new CNodo();
        NuevoNodo.dato=dat;
        int ban=0;
        if (Vacia()){
            NuevoNodo.sig=NuevoNodo;
            cabeza = NuevoNodo;
        }
        else {  if (dat<cabeza.dato) {
                    lue=cabeza;
                    ant=cabeza;
                        do{
                          ant=lue;
                          lue=lue.sig;
                        }while(lue!=cabeza);
                        ant.sig=NuevoNodo;
                        NuevoNodo.sig=cabeza;
                        cabeza = NuevoNodo;                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     
                }
                else {  ant=cabeza;
                        lue=cabeza;
                        while (ban==0){
                            if (dat>=lue.dato) {
                                ant=lue;
                                lue=lue.sig;
                            }
                            if (lue==cabeza){
                                ban=1;
                            }
                            else {
                                    if (dat<lue.dato){
                                        ban=1;
                                    }
                            }
                        }
                        ant.sig=NuevoNodo;
                        NuevoNodo.sig=lue;
                }
        }
    }
    public void Eliminar(int dat) {
        CNodo ant,lue;
        int ban=0;
        if (Vacia()) {   
            System.out.print("Lista vacía ");
        }
        else {  if (dat<cabeza.dato) {
                    System.out.print("dato no existe en la lista ");
                }
                else {
                        if (dat==cabeza.dato) {
                            lue=cabeza;
                            ant=cabeza;
                                while(ban==0){
                                    if(lue.sig==cabeza){
                                        ban=1;
                                    }
                                  ant=lue;
                                  lue=lue.sig;
                                }
                                    if(lue.sig==cabeza){
                                      cabeza=null;  
                                    }
                                    else{
                                        cabeza=cabeza.sig;
                                         ant.sig=cabeza;
                                    }
                        }
                        else {  ant=cabeza;
                                lue=cabeza;
                                while (ban==0) {
                                    if (dat>lue.dato) {
                                        ant=lue;
                                        lue=lue.sig;
                                    }
                                    else ban=1;
                                    if (lue==cabeza) {
                                        ban=1;
                                    }
                                    else {
                                            if (lue.dato==dat) 
                                                ban=1;
                                    }
                                }
                                if (lue==cabeza) {
                                    System.out.print("dato no existe en la Lista ");
                                }
                                else {
                                        if (dat==lue.dato) {
                                            ant.sig=lue.sig;
                                        }
                                        else 
                                            System.out.print("dato no existe en la Lista ");
                                }
                        }
                }
        }
    }

    public boolean Vacia() {
        return(cabeza==null);
    }

    public void Visualizar() {
        CNodo Temporal;
        Temporal=cabeza;
        if (!Vacia()) {
            do {
                System.out.print(" " + Temporal.dato +" ");
                Temporal = Temporal.sig;
            }while(Temporal!=cabeza);
            System.out.println("");
        }
        else
            System.out.println("Lista vacía");
    }
}


        CLista objLista= new CLista();
        System.out.println("Lista con datos");
        objLista.Insertar(10);
        objLista.Insertar(5);
        objLista.Insertar(8);
        objLista.Insertar(13);
        objLista.Insertar(1);
        objLista.Insertar(22);
        objLista.Visualizar();
        System.out.println("Lista sin dato ");
        objLista.Eliminar(8);
        objLista.Eliminar(5);
        objLista.Visualizar();
    }
}